# Achievement Tracker

## 2023 Achievements

- Shipped [Support `environment` keyword with `trigger` keyword (Parent-child/Multi-project pipelines)](https://gitlab.com/groups/gitlab-org/-/epics/8483)

## On-going projects

- [The `Service` data model - blueprint](https://gitlab.com/gitlab-org/gitlab/-/issues/420614)
- [Runway collaboration](https://gitlab.com/gitlab-com/gl-infra/platform/runway/team/-/issues/26)
  - [Allow Service project to select deployment environment and customize CD workflow](https://gitlab.com/gitlab-com/gl-infra/platform/runway/team/-/issues/36)
